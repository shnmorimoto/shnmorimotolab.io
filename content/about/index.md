---
title: About
date: 2021-12-29T09:08:00+09:00
description: about page
type: about
---

### Shinichi Morimoto

Software Developer, mainly developing the back end of web application.
Livingin in Tokyo, Japan.

- [Github](https://github.com/shnmorimoto)
- [Twitter](https://twitter.com/shnmorimoto)
- [Linkedin](https://www.linkedin.com/in/shinichi-morimoto-905b9b18a/)
