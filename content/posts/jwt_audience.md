---
title: "JwtのAudienceについて諸々"
date: 2022-04-10T22:22:02+09:00
archives: ["2022/04"]
draft: false
tags: ["jwt", "go"]
---

仕事で少し[Auth0](https://auth0.com/jp)を触っている。
今の会社では BackEnd に Go を採用しているので、jwt の検証には Auth0 が提供している[go-jwt-middleware](https://github.com/auth0/go-jwt-middleware)を利用しているのだが、Audience の検証の挙動が少し意図しない動きになっていた。

```go
// Set up the validator.
	jwtValidator, err := validator.New(
		keyFunc,
		validator.HS256,
		"https://<issuer-url>/",
		[]string{"<audience>"},
	)
```

こんな感じで、複数の audience を設定できるようになっているものの、現状の validation はクライアント側の jwt が指定した audience を全て持っている場合、いわゆる all match の場合のみ検証が通る実装になっている。

個人的には any match で検証が通ると思っていたので、少し驚いた。

go-jwt-middleware は jwt の検証に内部では go-jose を利用しており、同様の問題に困っている方が issue を上げていたが、放置されている。（RFC も見たものの、any match と all match どちらが正しいか読み取れなかった。）

<iframe class="hatenablogcard" style="width:100%;height:155px;margin:15px 0;max-width:680px;" title="Multiple audiences and JWT validation · Issue #286 · square/go-jose · GitHub" src="https://hatenablog-parts.com/embed?url=https://github.com/square/go-jose/issues/286" frameborder="0" scrolling="no"></iframe>

go-jose は square 社が管理している OSS だが、メインメンテナの方が square に現在所属していないようで、次期リリースバージョンの v3 からコミュニティに移行するみたいである。そのため、現行の v2 はメンテナンスモードに移行しており、issue や PR は放置されている状態のように見える。v3 の方も動きがないようだが...

実際 PR を上げてみたものの、現在絶賛レスポンスがない状態になっている。

<iframe class="hatenablogcard" style="width:100%;height:155px;margin:15px 0;max-width:680px;" title="Fix Allow multiple audiences by shnmorimoto · Pull Request #369 · square/go-jose · GitHub" src="https://hatenablog-parts.com/embed?url=https://github.com/square/go-jose/pull/369" frameborder="0" scrolling="no"></iframe>

ちなみに go-jwt-middleware の方にも issue を上げているが、こちらもレスポンスがない。

<iframe class="hatenablogcard" style="width:100%;height:155px;margin:15px 0;max-width:680px;" title="If we set multiple audience, we get the authentication error. · Issue #148 · auth0/go-jwt-middleware · GitHub" src="https://hatenablog-parts.com/embed?url=https://github.com/auth0/go-jwt-middleware/issues/148" frameborder="0" scrolling="no"></iframe>

ということで、困っているのだが、ネクストアクションが思いつかず、どうしたものかなという状態で止まっている。

幸いなことに、特に今すぐ直してもらわないといけないということでもないので、果報は寝て待ての精神で放置している。
