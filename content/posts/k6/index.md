---
title: "負荷試験ツールとしてk6を利用する"
date: 2022-09-04T18:55:05+09:00
archives: ["2022/09"]
draft: false
tags: ["tool", "js"]
---

SheepMedical では負荷試験ツールとして k6 を利用しています。

<iframe class="hatenablogcard" style="width:100%;height:155px;max-width:680px;" title="Load testing for engineering teams | Grafana k6" src="https://hatenablog-parts.com/embed?url=https://k6.io/" width="300" height="150" frameborder="0" scrolling="no"></iframe>

k6 は Grafana Labs がオープンソースで開発している負荷試験ツールで下記のような特徴があります。

- Javascript でスクリプトを作成することができる
- Go で作成されているため、バイナリを置くだけで、利用することができる
- 負荷試験結果を様々なフォーマットで export できる
- SaaS も用意されており、複数リージョンから実行したい場合などに、ローカルで作成したスクリプトをクラウドで実行することができる

簡易に利用できることや、特に別途 DSL を覚える必要がなく、JS でスクリプトを作成できることから採用しました。

## インストール

各種環境用にパッケージが用意されているため、簡単にインストールすることができます。

<iframe class="hatenablogcard" style="width:100%;height:155px;max-width:680px;" title="Installation" src="https://hatenablog-parts.com/embed?url=https://k6.io/docs/getting-started/installation/" width="300" height="150" frameborder="0" scrolling="no"></iframe>

Github のリリースページにバイナリがありますので、下記のように負荷試験環境にバイナリを落としてくるだけでも利用できます。

```shell
$ curl -OL https://github.com/grafana/k6/releases/download/v0.39.0/k6-v0.39.0-macos-arm64.zip
$ tar xvzf k6-v0.39.0-macos-arm64.zip
```

<iframe class="hatenablogcard" style="width:100%;height:155px;max-width:680px;" title="Releases · grafana/k6" src="https://hatenablog-parts.com/embed?url=https://github.com/grafana/k6/releases" width="300" height="150" frameborder="0" scrolling="no"></iframe>

## 実行

前述のように、スクリプトは JS で書くことができます。

```Javascript
import http from 'k6/http';
import { sleep } from 'k6';

export default function () {
  http.get('http://localhost:8080');
  sleep(1);
}
```

実行は作成した JS ファイルを指定するだけです。

```shell
$ k6 run script.js
```

k6 では並列度として、VUs (Virtual Users)があります。実行時に指定することができます。

```shell
$ k6 run script.js --vus 10
```

負荷実行期間は Duration で指定可能です。

```shell
$ k6 run script.js --duration 30s
```

上記２つはスクリプト内でも指定可能です。

```Javascript
import http from 'k6/http';
import { sleep } from 'k6';

export const options = {
  vus: 10,
  duration: '30s',
};

export default function () {
  http.get('http://localhost:8080');
  sleep(1);
}
```

## 基本的な構造

スクリプトは下記のような構造を持っています。

```Javascript
export function setup() {
// 起動時に一回だけ実行される
};

export default function(tokens) {
// 実行期間中に繰り返し実行される
};

export function teardown() {
// 終了時に実行される
};
```

実行には少なくとも `default`が export されている必要があります。
setup に前処理を記載し、default には負荷試験内容、teardown に後処理を記載する形になります。

## 負荷試験結果

スクリプトを実行すると通常は stdout に結果が表示されます。([ドキュメントより](https://k6.io/docs/getting-started/results-output/))

![k6 result](k6-results-stdout.png)

`out` オプションにより様々なフォーマットでエクスポートできます。

```shell
$ k6 run --out statsd script.js
```

出力できるタイプの一覧はドキュメントに記載があります。

<iframe class="hatenablogcard" style="width:100%;height:155px;max-width:680px;" title="Results output" src="https://hatenablog-parts.com/embed?url=https://k6.io/docs/getting-started/results-output/#external-outputs" width="300" height="150" frameborder="0" scrolling="no"></iframe>

またスクリプト内に `handleSummary` を定義することで出力する内容をカスタマイズすることができます。

SheepMedical では結果を HTML レポートとして参照したいため、 `k6-reporter` を利用してカスタマイズしています。

```Javascript
export function handleSummary(data) {
    return {
      'stdout': textSummary(data, { indent: ' ', enableColors: true }),
      "summary.html": htmlReport(data)
    };
};
```

<iframe class="hatenablogcard" style="width:100%;height:155px;max-width:680px;" title="benc-uk/k6-reporter: Output K6 test run results as formatted & easy to read HTML reports" src="https://hatenablog-parts.com/embed?url=https://github.com/benc-uk/k6-reporter" width="300" height="150" frameborder="0" scrolling="no"></iframe>

## シナリオ

Ramping up しながら、徐々に負荷を増やしていきたいケースなどあると思います。

k6 では executor という形で様々なシナリオを実現できます。

<iframe class="hatenablogcard" style="width:100%;height:155px;max-width:680px;" title="Scenarios" src="https://hatenablog-parts.com/embed?url=https://k6.io/docs/using-k6/scenarios/" width="300" height="150" frameborder="0" scrolling="no"></iframe>

シナリオは option で指定します。例として、Warm up する場合は下記のような形になります。

```Javascript
//warmup/ramp testの並列度、実行時間の設定
export let options = {
    scenarios: {
        ramping_up_scenario: {
            executor: 'ramping-vus',

            startVUs:1,
            // 1 並列実行60s
            // target count 実行 rampup秒
            // target count実行 target秒
            stages: [
              {duration: '60s', target: 1},
              {duration: config.RAMPUP_DURATION, target: config.TARGET_PARALLELISM_COUNT},
              {duration: config.TARGET_DURATION, target: config.TARGET_PARALLELISM_COUNT},
            ]
        }
    }
};
```

## ファイルからデータ読み込み

User の ID/PASS を事前に用意しておき、それらを利用して、認証してからリクエストを送りたい場合があると思います。

k6 では JS でスクリプトが記載できるので、このようなケースでも簡単に実現できます。

```Javascript
//jsonファイルから、userとpasswordを取得
const user_data_from_file = new SharedArray('user_data', function () {
    return JSON.parse(open('./user_data.json')).users;
});

let tokens = [];

export function setup() {
    //実行開始時にtokenを取得
    user_data_from_file.forEach(user => {
        // id/passからtokenを取得するget_tokenを別途定義しておく
        const token = get_token(user.username, user.password);
        tokens.push(token);
        sleep(2);
    });
    return tokens;
};

export default function(tokens) {

    //tokenをランダムに取得
    const token = tokens[Math.floor(Math.random() * tokens.length)];
    ...

};
```

## まとめ

使ってみた印象としては、負荷試験に必要な機能が揃っており、また主力結果などをカスタマイズできるため、柔軟に様々なことを実現できるために、非常に使いやすいと感じました。
別途 DSL を覚える必要がないところも嬉しい点ですね。

またドキュメントも充実しており、ほとんどの問題がドキュメントを参照することで解決できました。
