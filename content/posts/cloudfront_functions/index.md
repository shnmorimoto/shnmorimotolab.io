---
title: "CloudFront Functionsをリダイレクトに利用してみた"
date: 2022-09-12T20:39:28+09:00
archives: ["2022/09"]
draft: false
tags: ["aws"]
---

CloudFront Functions を業務で少し利用したので、簡単に紹介したいと思います。

## 実現したいこと

事業部の方からとあるドメインが繋がらないと相談を受けました。

DNS 自体は自社で管理していたのですが、サービスについてはホスティングサービスを利用しており、その他諸々の事情で DNS で直接対象のサーバー/IP を設定することができなかったため、リダイレクトで対応することにしました。

## リダイレクト方法

自社では AWS を利用していることもあり、リダイレクトについては、下記の方法が考えられます。

- EC2 を立てて、nginx 等を動かす
- API Gateway + Lambda
- Lambda@Edge
- CloudFront Functions

あまり構築や運用に手間をかけたくなかったので、今回は Lambda@Edge か CloudFront Functions で対応することにしました。

## Lambda@Edge と CloudFront Functions

Lambda@Edge と CloudFront Functions は両方とも Edge Location で稼働します。([Amazon Web Services ブログより](https://aws.amazon.com/jp/blogs/news/introducing-cloudfront-functions-run-your-code-at-the-edge-with-low-latency-at-any-scale/))

![lambda@edgeとCloudFront Functions](./aws-edge.png)

違いとしては、上記の通り Lambda@Edge は Region の Edge Location で稼働し、CloudFront Functions の CloudFront と同じ Edge Location で稼働します。その他にも下記のように実行できるランタイムなどの制限の違いがあります。([AWS Web Services ブログより](https://aws.amazon.com/jp/blogs/news/introducing-cloudfront-functions-run-your-code-at-the-edge-with-low-latency-at-any-scale/))

![lambda@edgeとCloudFront Functionsのスペック](./aws-edge-spec.png)

リダイレクト処理のみのため、最大実行時間、メモリは問題にならず、また無料利用枠があることから、今回は CloudFront Functions を利用することにしました。

実際には CloudFront Functions は毎回実行されるのに対して、Lambda@Edge では CloudFront 側でキャッシュが効くため、ユースケースによっては実行回数が抑えられる場合がありますので、単純には料金比較はできないのですが、今回のケースではそこまでアクセスがあるわけではなかったため、CloudFront Functions にしています。

<iframe class="hatenablogcard" style="width:100%;height:155px;margin:15px 0;max-width:680px;" title="CloudFront FunctionsはLambda@Edgeより安い。それ本当？！ | DevelopersIO" src="https://hatenablog-parts.com/embed?url=https://dev.classmethod.jp/articles/sometimes-cf2-price-is-higher-than-lae/" frameborder="0" scrolling="no"></iframe>

## CloudFront Functions の設定

チュートリアルの通り、設定自体は簡単で CloudFront 作成後、メニューの `関数` から作成することができます。

![create_function](./create_function.png)

<iframe class="hatenablogcard" style="width:100%;height:155px;margin:15px 0;max-width:680px;" title="チュートリアル: CloudFront Functions を使用した単純な関数の作成 - Amazon CloudFront" src="https://hatenablog-parts.com/embed?url=https://docs.aws.amazon.com/ja_jp/AmazonCloudFront/latest/DeveloperGuide/functions-tutorial.html" frameborder="0" scrolling="no"></iframe>

`関数コード` にてコードを記述していきます。

現在サポートされているランタイムは ECMA Script 5.1 準拠ということで、少々古く感じますが、一部 6~9 の機能や独自機能もサポートされており、そこまで困ることはないかと思います。

![function](./function.png)

<iframe class="hatenablogcard" style="width:100%;height:155px;margin:15px 0;max-width:680px;" title="CloudFront Functions の JavaScript runtime 機能 - Amazon CloudFront" src="https://hatenablog-parts.com/embed?url=https://docs.aws.amazon.com/ja_jp/AmazonCloudFront/latest/DeveloperGuide/functions-javascript-runtime-features.html" frameborder="0" scrolling="no"></iframe>

`テスト` から事前に記述したコードをテストすることができます。

![test](./test.png)

テストは変更したコードである `Development` と現在公開されているバージョンである `live` の 2 つをテストすることができます。

![test](./test-2.png)

テストして問題がなければ、 `公開` から最新バージョンとして公開することができます。

![publish](./publish.png)

公開後、既存の CloudFront の behavior に関連付けを行います。この操作により、CloudFront にアクセスがあった場合に CloudFront Functions が実行されるようになります。

![assosiation](./assosiation.png)

terraform でも CloudFront Functions はサポートされているので、下記のように設定することができます。

```cf.tf
resource "aws_cloudfront_distribution" "main" {
    ...
     function_association {
      event_type   = "viewer-request"
      function_arn = aws_cloudfront_function.main.arn
    }
    ...
}

resource "aws_cloudfront_function" "main" {
  name    = "function"
  runtime = "cloudfront-js-1.0"
  comment = "redirect"
  publish = true
  code    = file("./function.js")
}
```

```function.js
function handler(event) {
    var response = {
        statusCode: 301,
        statusDescription: 'Moved Permanently',
        headers: {
            'location': { value: 'https://example.com' }
        }
    };
    return response;
}
```

<iframe class="hatenablogcard" style="width:100%;height:155px;margin:15px 0;max-width:680px;" title="aws_cloudfront_function | Resources | hashicorp/aws | Terraform Registry" src="https://hatenablog-parts.com/embed?url=https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudfront_function" frameborder="0" scrolling="no"></iframe>

## まとめ

実際に利用してみて、思ったより簡単に設定できるなと感じました。
CloudFront 上で実行されるため、運用負荷も少なく、簡易な処理であれば、どんどん使っていきたいと思いました。

Edge 上で動くため、様々な制限はあるものの、活用できる場面は多いかなと思うので、これからも適材適所で利用していきたいと思います。
