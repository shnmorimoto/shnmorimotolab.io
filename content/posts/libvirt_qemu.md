---
title: "libvirt+QEMUを使ってみた"
date: 2022-04-16T12:54:19+09:00
archives: ["2022/04"]
draft: false
tags: ["qemu"]
---

[containerd](https://containerd.io/) に出した PR が merge された。

<iframe class="hatenablogcard" style="width:100%;height:155px;margin:15px 0;max-width:680px;" title="fix pool_device_test by shnmorimoto · Pull Request #6807 · containerd/containerd · GitHub" src="https://hatenablog-parts.com/embed?url=https://github.com/containerd/containerd/pull/6807" frameborder="0" scrolling="no"></iframe>

修正自体は issue でも提示されている通り、テストを少し変更するだけでなので簡単。ただ ppc64le 上で確認する必要があったため、環境を用意するのが難しかった。[^*1]
簡単な issue にも関わらず、修正されていないのもそれが理由な気がする。

実マシンを用意するのはもちろんできないから、今回 QEMU を利用して確認した。

Linux を使っているなら下記の記事の通りすれば、簡単に ppc64le の環境を用意できる。

<iframe class="hatenablogcard" style="width:100%;height:155px;margin:15px 0;max-width:680px;" title="libvirtを使ってppc64leの検証環境を立ち上げる話 - 2020-10-20 - ククログ" src="https://hatenablog-parts.com/embed?url=https://www.clear-code.com/blog/2020/10/20.html" frameborder="0" scrolling="no"></iframe>

Ubuntu の ppc64le 版 iso については Ubuntu のサイトから落とすことができる。

<iframe class="hatenablogcard" style="width:100%;height:155px;margin:15px 0;max-width:680px;" title="Ubuntu 18.04.6 LTS (Bionic Beaver)" src="https://hatenablog-parts.com/embed?url=https://cdimage.ubuntu.com/releases/18.04/release/" frameborder="0" scrolling="no"></iframe>

実際の操作は virsh コマンドを利用する。

以下、備忘録代わりのメモ。

#### 仮想マシンの作成

```shell
$ sudo virt-install --name=ubuntu20-ppc64le --ram=4096 --vcpus=4 \
                    --os-type=linux --os-variant=ubuntu20.04 --arch=ppc64le \
                    --machine pseries-2.12 \
                    --disk=/var/lib/libvirt/images/ubuntu20-ppc64le.img,format=qcow2,size=20 \
                    --cdrom=ubuntu-20.04.4-live-server-ppc64el.iso
                    --serial=pty --console=pty --boot=hd --graphics=none
```

#### 仮想マシンの一覧表示

```shell
$ sudo virsh list --all
 Id   Name               State
-----------------------------------
 -    ubuntu18-ppc64le   shut off
 -    ubuntu20-ppc64le   shut off
```

#### 仮想マシンの起動

```shell
$ sudo virsh start ubuntu18-ppc64le
Domain 'ubuntu18-ppc64le' started
```

#### 仮想マシンに接続

```shell
$ sudo virsh console ubuntu18-ppc64le
Connected to domain 'ubuntu18-ppc64le'
Escape character is ^] (Ctrl + ])

Ubuntu 18.04.5 LTS ubuntu hvc0

ubuntu login:
```

#### 仮想マシンの停止

```shell
$ sudo virsh shutdown ubuntu18-ppc64le
Domain 'ubuntu18-ppc64le' is being shutdown
```

#### 仮想マシンの vcpu の変更

最初に最大 vcpu 数の変更

```shell
$ sudo virsh setvcpus ubuntu18-ppc64le 4 --config --maximum
```

その後 vcpu 数を変更

```shell
$ sudo virsh setvcpus ubuntu18-ppc64le 4 --config
```

--config をつけると次回起動時から有効化される

#### 仮想マシンのメモリを変更

最初に最大メモリ数を変更

```shell
sudo virsh setmaxmem ubuntu18-ppc64le 2G --config
```

その後メモリ数を変更

```shell
sudo virsh setmem ubuntu18-ppc64le 2G --config
```

[^*1]: 実際には Ubuntu20.04 での挙動の変更が原因だと思うけど。
