---
title: "半年で取り組んだAWS管理改善について"
date: 2022-08-29T17:20:12+09:00
archives: ["2022/08"]
draft: false
tags: ["aws"]
---

2 月から [SheepMedical 株式会社](https://www.sheepmedical.com/)にて SRE として働いています。入社してから半年程経ったので、これまで取り組んだ AWS 管理の改善についてご紹介します。

## 入社時点での問題点

会社としては既に AWS を利用して、数年経っている状態でしたが、専任のインフラエンジニアや SRE がおらず、下記のような問題点がありました。

- 管理アカウント配下や各 AWS アカウント配下に IAM 作成されており、IAM 管理が統一されていない
- 複数のプロダクトを展開しており、また海外展開していることもあり、AWS アカウントの数が約 40 と数が多い
- アカウントの数が多いため、各アカウント配下の構成がセキュアな構成になっているかどうか把握するのが難しい

## SSO の導入

上記で挙げた通り、IAM 管理が統一されておらず、どのアカウントにどのユーザーがログインできるのか管理するのが難しい状態だったため、AWS SSO(現 AWS IAM Identity Center)を導入しました。

<iframe class="hatenablogcard" style="width:100%;height:155px;margin:15px 0;max-width:680px;" title="AWS IAM Identity Center (successor to AWS SSO)" src="https://hatenablog-parts.com/embed?url=https://aws.amazon.com/jp/iam/identity-center/" frameborder="0" scrolling="no"></iframe>

また SSO を導入した段階で、Permission Set については Terraform 化をし、各利用者が Github の PR を通して申請する運用を導入しました。
この運用を導入したことで、各アカウントにどのユーザがログインできるのかが Github を見るだけで把握できるようになり、また誰が承認をしたのかも PR を通して把握することができるようになりました。

これらの SSO の導入が完了した段階で IAM の棚卸しを実施し、不要な IAM ユーザーを削除しました。

## AWS Contorl Tower の導入

Organization を導入していたものの、特に SCP などは設定されていませんでした。また各アカウントに CloudTrail の設定や AWS Config が設定されておらず、証跡ログがとれていない状態でした。
そのため、AWS Control Tower を導入しました。AWS Control Tower の詳細については、Classmethod 様の一連の記事が参考になります。

<iframe class="hatenablogcard" style="width:100%;height:155px;margin:15px 0;max-width:680px;" title="2021年末版Control Tower総まとめ〜これからAWSマルチアカウント管理したい人に向けて〜 #reinvent | DevelopersIO" src="https://hatenablog-parts.com/embed?url=https://dev.classmethod.jp/articles/aws-control-tower-allin-2021/" frameborder="0" scrolling="no"></iframe>

Control Tower を導入することで、下記を実現できました。

- ガードレールによって、各アカウントに必須の設定を強制することができる
  - 各アカウントの詳細を把握するのが難しいため、ガードレールは検出のみを有効にしています。後述する Security Hub のスコアが改善した段階で、今後徐々に予防的ガードレールを有効化したいと思っています。
- 上述の SSO も Control Tower 経由で有効化
- 全アカウントについて、CloudTrail と Config を有効化できる
- 下記のような Log Archive Account, Audit Accout が作成されマルチアカウントのベストプラクティスに則ったアカウント管理ができる([builders.flash より参照](https://aws.amazon.com/jp/builders-flash/202007/multi-accounts-best-practice/?awsf.filter-name=*all))

![マルチアカウント図](https://d1.awsstatic.com/Developer%20Marketing/jp/magazine/2020/img_multi-accounts-best-practice_06.88299bbff4cc99d0abc21d5566ae5d980908a925.png)

Control Tower では各アカウント毎に管理配下におくかどうかを決定できます。ガードレールは検出のみ有効化しており、既存のアカウントには影響がないと思われましたが、念の為、一度に適用するのではなく、dev 用のアカウントから始まり、影響がないのを確認しながら、数週間かけて全アカウントに適用していきました。

これまでは、誰がどのような理由で AWS アカウントを作成したのかの記録が残っていなかったり、AWS ルートアカウントの命名規則等がバラバラだったりと管理できていなかったため、AWS Contorl Tower Account Factory for Terraform(AFT)を導入し、Github 上の PR を通して、新規アカウントを作成する運用に変更しました。

<iframe class="hatenablogcard" style="width:100%;height:155px;margin:15px 0;max-width:680px;" title="新機能 – AWS Control Tower Account Factory for Terraform | Amazon Web Services ブログ" src="https://hatenablog-parts.com/embed?url=https://aws.amazon.com/jp/blogs/news/new-aws-control-tower-account-factory-for-terraform/" frameborder="0" scrolling="no"></iframe>

AFT は複雑なため導入は悩みましたが、新規 AWS アカウントの作成、承認を Github 上で管理するメリットを考慮し、一旦採用に踏み切りました。運用を通して、今後継続して利用するかどうか判断したいと思っています。

また Contorl Tower の導入と併せて、各アカウントのルートアカウントの命名規則の統一や MFA の設定、不要なアカウントの棚卸しを実施しています。

## nOps の導入

nOps はクラウド管理プラッフォームで構成チェックやコスト最適化チェックを検出してくれるサービスです。

<iframe class="hatenablogcard" style="width:100%;height:155px;margin:15px 0;max-width:680px;" title="AWS & MicroSoft Azure Cloud Management Platform | nOps" src="https://hatenablog-parts.com/embed?url=https://www.nops.io/" frameborder="0" scrolling="no"></iframe>

詳細については、こちらも Classmethod 様の記事が参考になります。

<iframe class="hatenablogcard" style="width:100%;height:155px;margin:15px 0;max-width:680px;" title="nOps の記事一覧 | DevelopersIO" src="https://hatenablog-parts.com/embed?url=https://dev.classmethod.jp/tags/nops/" frameborder="0" scrolling="no"></iframe>

nOps を利用することで、セキュアな構成になっているか確認することができ、またコスト最適化の観点でも不必要に大きなサイズのインスタンスが利用されていないか等のチェックができるようになりました。

特にコスト最適化の面では、実際に検証のため一時的に利用して停止し忘れていたインスタンスの検出することができ、役立っています。

また nOps にはレポート機能があり、Well-Architected や SOC2、HIPPA などに沿った構成かチェックすることもできます。現在はそこまで利用できていませんが、今後は活用していきたいと思っています。

運用としては各プロダクト担当者を含めた MTG を月一回開催しています。そこで構成のチェックや適切なインスタンスサイズが利用されているか等の確認をし、改善活動に活かしています。

現在は重点的に確認したい一部のアカウントのみを連携していますが、今後連携するアカウントを増やしたいと思っています。

## AWS Security Hub, GuardDuty, Amazon Inspector, AWS Compute Optimizer の導入

nOps と連携されていないアカウントのために、Security Hub を有効化しています。

<iframe class="hatenablogcard" style="width:100%;height:155px;margin:15px 0;max-width:680px;" title="AWS Security Hub（統合されたセキュリティ & コンプライアンスセンター）| AWS" src="https://hatenablog-parts.com/embed?url=https://aws.amazon.com/jp/security-hub/" frameborder="0" scrolling="no"></iframe>

Security Hub を有効化したことで、セキュアでない構成が可視化されたので、より良いスコアになるように地道に改善活動をしています。

併せて GuardDuty も有効化し、不審なアクティビティがあった際に気付けるようにしています。現在は Slack に通知するようにしており、通知があった際は都度確認しています。

最近のアップデートで Malware の検知ができる GuardDuty Malware Protection がリリースされたので、こちらも有効化しています。

<iframe class="hatenablogcard" style="width:100%;height:155px;margin:15px 0;max-width:680px;" title="Amazon GuardDuty がマルウェア対策機能を追加" src="https://hatenablog-parts.com/embed?url=https://aws.amazon.com/jp/about-aws/whats-new/2022/07/malware-protection-feature-amazon-guardduty/" frameborder="0" scrolling="no"></iframe>

いくつかのプロダクトでは EC2 を利用していることもあり、nOps や Security Hub で検知できない OS レベルの脆弱性検知として、AWS Inspector を有効化しています。Amazon System Mananger を有効化するだけで簡単に導入でき、脆弱性があるインスタンスを検知することができます。

<iframe class="hatenablogcard" style="width:100%;height:155px;margin:15px 0;max-width:680px;" title="Amazon Inspector（アプリのセキュリティとコンプライアンスの改善をサポート）| AWS" src="https://hatenablog-parts.com/embed?url=https://aws.amazon.com/jp/inspector/" frameborder="0" scrolling="no"></iframe>

コスト最適化の面では EC2 インスタンスのみが対象のものの AWS Optimizer を有効化しています。

<iframe class="hatenablogcard" style="width:100%;height:155px;margin:15px 0;max-width:680px;" title="AWS Compute Optimizer (ワークロードに最適なコンピューティングリソースを特定) | AWS" src="https://hatenablog-parts.com/embed?url=https://aws.amazon.com/jp/compute-optimizer/" frameborder="0" scrolling="no"></iframe>

こちらも不必要に大きなインスタンスが利用されていないか定期的に確認しています。

## まとめ

その他細々とした機能の導入をしているものの、代表的なものについてご紹介しました。
これらの機能は簡単に導入できるため、実作業としては難しくないものの、各アカウントでどのようなサービスが動いているのかの調査・把握、また一部のプロダクトや海外向けのプロダクトについては、協力会社、海外支社にて開発・運用を任していることもあり、それらとの調整に主に時間がかかったかなと思います。

一通りの機能は導入できたかと思いますので、今後は運用を通して、Security Hub のスコアの継続的な改善やより効率の良い運用方法の確立などに注力していきたい思っています。
