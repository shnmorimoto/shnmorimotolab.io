---
title: "Kubernetes Upstream Trainingに参加した"
date: 2021-12-14T11:35:40+09:00
draft: false
tags: ["kubernetes"]
archives: ["2021/12"]
---

OSS Japan + ALS のイベントとして 12/13 に開催された Kubernetes Upstream Training に参加した。

<iframe class="hatenablogcard" style="width:100%;height:155px;margin:15px 0;max-width:680px;" title="スケジュールSchedule | Linux Foundation Events" src="https://hatenablog-parts.com/embed?url=https://events.linuxfoundation.org/archive/2021/open-source-summit-japan/program/schedule/" frameborder="0" scrolling="no"></iframe>

CNDT などでも開催されているイベントで、実際の contributor が講師ということで、以前から興味があったが、今回時間ができたので、参加してみた。

トレーニングは座学とハンズオンで構成されており、座学では Kubernetes における SIG や WG などのコミュニティの概要や、実際に contribute する際のコミュニケーションの方法、また各 contributor による各プロジェクトの環境構築の方法/注意事項などが聞けたので、あまり k8s の知識がない私でも概要や contribute する際のおおよその流れを理解することができた。

すこし話はそれるけれど、一番最初に CNCF の [Hippie Hacker](https://twitter.com/hippiehacker) さんから k8s コミュニティのお話があり、スライドを利用しない紙芝居？形式のようなプレゼンでとても面白かった。

概要を知りたいは下記に一部の資料がまとまっているので、参加した際のイメージが掴めるかもしれない。

<iframe class="hatenablogcard" style="width:100%;height:155px;margin:15px 0;max-width:680px;" title="contributor-playground/japan/assets at master · kubernetes-sigs/contributor-playground · GitHub" src="https://hatenablog-parts.com/embed?url=https://github.com/kubernetes-sigs/contributor-playground/tree/master/japan/assets" frameborder="0" scrolling="no"></iframe>

ハンズオンでは Playground という環境を利用して、PR の作成からレビュー、マージまでの一通りの流れを体験することができた。かなり丁寧に説明して頂けたので、普段から git の操作や Github に慣れていれば、特に問題なくハンズオンができると思う。

<iframe class="hatenablogcard" style="width:100%;height:155px;margin:15px 0;max-width:680px;" title="GitHub - kubernetes-sigs/contributor-playground" src="https://hatenablog-parts.com/embed?url=https://github.com/kubernetes-sigs/contributor-playground" frameborder="0" scrolling="no"></iframe>

実際に contribute している講師の方から座学もハンズオンもかなり丁寧に説明頂けた上、空き時間などには質問の時間をかなりとって頂けるので、contribute する際の第一歩としてとても有意義なトレーニングだった。

講師の方に感謝するとともに、時間をみつけて、ぜひなにか contribute してみたいと思う。
